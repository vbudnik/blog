<?php

namespace App\Http\Controllers\Admin;

use App\Categori;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CategoiesController extends Controller
{
    public function index()
    {
        $categories = Categori::all();
        return view('admin.categories.index', ['categories' => $categories]);
    }

    public function create()
    {
        return view('admin.categories.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
           'title' => 'required'
        ]);
        Categori::create($request->all());
        return redirect()->route('categories.index');
    }

    public function edit($id)
    {
        $category = Categori::find($id);
        return view('admin.categories.edit', ['categori' => $category]);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required'
        ]);
        $category = Categori::find($id);
        $category->update($request->all());

        return redirect()->route('categories.index');
    }

    public function destroy($id)
    {
        Categori::find($id)->delete();
        return redirect()->route('categories.index');
    }
}
